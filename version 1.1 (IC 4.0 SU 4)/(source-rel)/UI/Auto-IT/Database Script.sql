/****** Object:  Table [dbo].[Com_Class]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Com_Class](
	[Communication_ID] [int] NOT NULL,
	[Classification_ID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Entered] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Com_Attr]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Com_Attr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Communication_ID] [int] NOT NULL,
	[Attribute_ID] [int] NOT NULL,
	[Process_ID] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Classifications]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classifications](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Priority] [int] NOT NULL,
	[ID_Parent] [int] NULL,
	[Process_ID] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Classifications] ON
INSERT [dbo].[Classifications] ([ID], [Priority], [ID_Parent], [Process_ID]) VALUES (61, 1, NULL, NULL)
INSERT [dbo].[Classifications] ([ID], [Priority], [ID_Parent], [Process_ID]) VALUES (62, 1, NULL, NULL)
INSERT [dbo].[Classifications] ([ID], [Priority], [ID_Parent], [Process_ID]) VALUES (63, 1, NULL, NULL)
INSERT [dbo].[Classifications] ([ID], [Priority], [ID_Parent], [Process_ID]) VALUES (64, 1, NULL, NULL)
INSERT [dbo].[Classifications] ([ID], [Priority], [ID_Parent], [Process_ID]) VALUES (65, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Classifications] OFF
/****** Object:  Table [dbo].[Class_Attrib]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class_Attrib](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Classification_ID] [int] NOT NULL,
	[Attribute_ID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Class_Attrib] ON
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (30, 61, 27)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (31, 61, 28)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (32, 61, 29)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (33, 61, 30)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (34, 61, 31)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (35, 61, 32)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (36, 61, 33)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (37, 61, 34)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (38, 61, 35)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (39, 61, 36)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (40, 61, 37)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (41, 61, 38)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (42, 61, 39)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (43, 61, 40)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (44, 63, 41)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (45, 63, 42)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (46, 63, 43)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (47, 63, 44)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (48, 63, 45)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (49, 63, 46)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (50, 63, 47)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (51, 63, 48)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (52, 63, 49)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (53, 63, 50)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (54, 63, 51)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (55, 63, 52)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (56, 63, 53)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (57, 63, 54)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (58, 63, 55)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (59, 61, 56)
INSERT [dbo].[Class_Attrib] ([ID], [Classification_ID], [Attribute_ID]) VALUES (60, 61, 57)
SET IDENTITY_INSERT [dbo].[Class_Attrib] OFF
/****** Object:  Table [dbo].[Attributes]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attributes](
	[ID_Parent] [int] NULL,
	[Entity_ID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Process_ID] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Attributes] ON
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 27, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 28, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 29, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 30, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 31, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 3, 32, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 33, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 3, 34, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 3, 35, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 3, 36, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 37, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 38, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 39, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 40, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 41, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 42, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 43, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 44, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 45, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 46, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 47, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 48, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 49, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 50, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 51, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 52, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 53, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 54, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 55, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 56, NULL)
INSERT [dbo].[Attributes] ([ID_Parent], [Entity_ID], [ID], [Process_ID]) VALUES (NULL, 5, 57, NULL)
SET IDENTITY_INSERT [dbo].[Attributes] OFF
/****** Object:  Table [dbo].[Values_Text]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Values_Text](
	[GUID] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](250) NULL,
	[Process_ID] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Values_Numerics]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Values_Numerics](
	[GUID] [uniqueidentifier] NOT NULL,
	[Value] [int] NULL,
	[Process_ID] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Values_Memos]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Values_Memos](
	[GUID] [uniqueidentifier] NOT NULL,
	[Process_ID] [nvarchar](25) NULL,
	[Value] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Values]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Values](
	[Context_GUID] [uniqueidentifier] NOT NULL,
	[Signature] [char](64) NULL,
	[Parent_GUID] [uniqueidentifier] NULL,
	[Record_ID] [int] NOT NULL,
	[Record_GUID] [uniqueidentifier] NULL,
	[Filter_ID] [int] NULL,
	[Log_GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__Values__603A0C60532343BF] PRIMARY KEY CLUSTERED 
(
	[Record_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sample]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sample](
	[SampleID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NULL,
	[Order_Number] [int] NULL,
	[Name_First] [varchar](255) NULL,
	[Name_Last] [varchar](255) NULL,
	[Email_Address] [varchar](255) NULL,
	[Phonenumbr_01] [varchar](100) NULL,
	[PhoneNumbr_02] [varchar](100) NULL,
	[PhoneNumbr_Prefer] [int] NULL,
	[Activity_ID] [int] NULL,
	[Activity_Date] [char](25) NULL,
	[Activity_Description] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[SampleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Sample] ON
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (5, 1, 663227, N'Fletcher', N'Short', N'dictum.Proin@nuncinterdum.ca', N'273-161-5073', N'704-316-1678', 2, 94685627, N'2014-05-04T01:49:05-07:00', N'magnis dis parturient')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (6, 2, 520546, N'Charde', N'Salinas', N'euismod@iaculislacus.com', N'279-295-3646', N'705-500-7127', 2, 21786397, N'2014-06-06T04:03:46-07:00', N'non, egestas a, dui.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (7, 3, 718355, N'Cherokee', N'Jensen', N'varius@eu.co.uk', N'358-283-5361', N'361-803-7608', 2, 14886149, N'2014-01-18T21:33:40-08:00', N'malesuada fames ac')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (8, 4, 903673, N'Teagan', N'Perez', N'Duis@enim.edu', N'703-337-1829', N'881-109-1241', 1, 42589977, N'2014-10-31T22:35:16-07:00', N'at, egestas a, scelerisque sed,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (9, 5, 679690, N'Nash', N'Langley', N'ut.nulla@Phasellusfermentum.ca', N'700-197-4896', N'814-797-5531', 2, 62040387, N'2014-07-17T15:58:52-07:00', N'nibh. Quisque nonummy ipsum non')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (10, 6, 552199, N'Diana', N'Payne', N'auctor@Sedneque.edu', N'836-100-8311', N'235-461-7676', 2, 75600768, N'2014-01-07T13:00:43-08:00', N'parturient montes, nascetur ridiculus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (11, 7, 759749, N'Dakota', N'Brewer', N'et@eu.co.uk', N'631-261-9479', N'657-280-9697', 2, 45307028, N'2014-07-30T12:48:26-07:00', N'Curabitur consequat, lectus sit')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (12, 8, 429973, N'Christian', N'Avery', N'luctus@afeugiat.com', N'229-999-3080', N'868-202-5490', 1, 43672628, N'2014-11-20T21:52:07-08:00', N'leo elementum sem, vitae aliquam')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (13, 9, 112321, N'Nicholas', N'Slater', N'venenatis@elitpharetraut.org', N'160-421-0645', N'343-705-4602', 1, 23085937, N'2014-01-13T15:39:05-08:00', N'vulputate eu, odio. Phasellus at')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (14, 10, 169785, N'Courtney', N'Browning', N'tempus@adipiscing.com', N'923-467-7773', N'960-962-3021', 2, 79425186, N'2014-03-23T10:40:23-07:00', N'eros turpis non enim. Mauris')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (15, 11, 715408, N'Leroy', N'Mckay', N'nec.tempus.mauris@ultricesDuisvolutpat.net', N'393-890-3856', N'923-167-1226', 1, 26995037, N'2014-02-01T16:17:37-08:00', N'risus. In mi pede, nonummy')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (16, 12, 895671, N'Leanore', N'Jones', N'actonempus@adipiscingi.com', N'589-505-507', N'564-140-1510', 2, 68908117, N'2014-11-26T03:38:52-08:00', N'nisl elementum purus,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (17, 13, 847491, N'Jenna', N'Castaneda', N'risus@tinciduntdui.ca', N'282-798-8330', N'643-244-7683', 1, 58390458, N'2014-02-07T09:04:18-08:00', N'elementum, dui quis accumsan')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (18, 14, 455033, N'Dale', N'Cleveland', N'libero@sem.com', N'439-653-5959', N'128-887-9151', 1, 19537532, N'2014-09-30T16:50:04-07:00', N'Vivamus euismod urna.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (19, 15, 377108, N'Gemma', N'Patton', N'amet.luctus@loremauctorquis.edu', N'170-113-7022', N'925-536-3128', 2, 71044677, N'2014-10-01T09:08:09-07:00', N'arcu. Vestibulum ut')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (20, 16, 675592, N'Jada', N'Finley', N'vel.pede@Sed.com', N'971-263-6453', N'993-849-3895', 2, 81430272, N'2014-10-27T03:17:42-07:00', N'luctus felis purus ac tellus.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (21, 17, 843089, N'Simon', N'Cote', N'enim.condimentum@nuncinterdum.com', N'213-562-2799', N'277-564-6755', 1, 87137393, N'2014-12-23T04:07:49-08:00', N'magna. Sed eu eros. Nam')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (22, 18, 526943, N'Olympia', N'Aguirre', N'aliquet.magna.a@sapiengravidanon.com', N'897-560-4809', N'908-770-0621', 2, 14218182, N'2014-10-17T02:09:34-07:00', N'id, mollis nec, cursus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (23, 19, 963033, N'Alma', N'Henson', N'morbi.tristique@imperdietnon.co.uk', N'380-957-7350', N'187-552-0034', 2, 37130910, N'2014-09-04T07:38:48-07:00', N'volutpat ornare, facilisis eget,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (24, 20, 215479, N'Orlando', N'Delgado', N'nunc@velitin.edu', N'230-933-8047', N'223-962-4766', 2, 69406968, N'2014-11-21T11:26:36-08:00', N'mattis ornare, lectus ante')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (25, 21, 669342, N'Pamela', N'Lambert', N'ornare@egestasa.edu', N'997-488-6762', N'716-232-7577', 1, 64505176, N'2014-12-21T01:58:16-08:00', N'tempus risus. Donec egestas.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (26, 22, 152719, N'Brady', N'Orr', N'dictum.Phasellus.in@leoCras.com', N'126-490-9956', N'660-912-5424', 1, 67549186, N'2014-07-08T05:37:57-07:00', N'libero. Integer in magna. Phasellus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (27, 23, 944761, N'Nadine', N'Woodward', N'non.luctus.sit@blandit.ca', N'479-868-9116', N'908-224-5927', 1, 41943868, N'2014-04-27T21:39:39-07:00', N'ac arcu. Nunc')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (28, 24, 436172, N'Whoopi', N'Conrad', N'egestas.ligula.Nullam@tellus.co.uk', N'688-423-0006', N'397-685-1986', 2, 40812099, N'2014-07-27T09:11:52-07:00', N'lobortis. Class aptent')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (29, 25, 233674, N'Keith', N'Chandler', N'ultricies.dignissim.lacus@elementum.ca', N'799-845-5849', N'666-890-1719', 1, 72041585, N'2014-06-13T06:53:57-07:00', N'dolor dolor, tempus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (30, 26, 786009, N'Halla', N'Bean', N'sit.amet.faucibus@NullamnislMaecenas.edu', N'158-347-6141', N'739-273-8656', 2, 94318382, N'2014-07-03T17:07:21-07:00', N'dui, semper et, lacinia')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (31, 27, 865917, N'Zelenia', N'Calderon', N'amet.consectetuer@augue.ca', N'488-247-9781', N'673-423-8778', 1, 59122278, N'2014-02-05T14:56:28-08:00', N'Praesent luctus. Curabitur egestas')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (32, 28, 739465, N'Ursa', N'Campbell', N'imperdiet@magnis.co.uk', N'261-794-8205', N'463-347-9948', 2, 11565321, N'2014-03-19T18:34:39-07:00', N'arcu. Morbi sit amet')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (33, 29, 258415, N'Ryder', N'Solomon', N'dui@pulvinar.net', N'840-387-8488', N'783-297-1537', 2, 29370402, N'2014-07-22T23:07:35-07:00', N'fermentum arcu. Vestibulum ante')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (34, 30, 754842, N'Leroy', N'Williamson', N'ornare.sagittis@ornaretortorat.org', N'859-889-6754', N'437-902-2069', 1, 32729163, N'2014-11-29T01:25:14-08:00', N'aliquet, sem ut cursus luctus,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (35, 31, 121171, N'Remedios', N'Shepherd', N'dui.augue.eu@nibh.net', N'627-904-8521', N'324-276-4892', 2, 71180717, N'2014-04-02T07:37:26-07:00', N'ut, molestie in, tempus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (36, 32, 625353, N'Abel', N'Murray', N'Nullam.lobortis.quam@condimentumDonec.org', N'725-662-0364', N'507-119-0049', 2, 83987880, N'2014-05-04T11:37:13-07:00', N'montes, nascetur ridiculus mus.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (37, 33, 382791, N'Garrison', N'Raymond', N'ornare.placerat@consequat.org', N'362-303-7002', N'591-846-9524', 1, 87914294, N'2014-07-21T13:07:50-07:00', N'in molestie tortor nibh')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (38, 34, 158140, N'Patrick', N'Lancaster', N'Morbi@ullamcorperDuis.com', N'132-728-3782', N'506-891-3415', 2, 93631229, N'2014-04-29T08:53:09-07:00', N'Nunc ullamcorper, velit')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (39, 35, 716294, N'Margaret', N'Summers', N'mi.lorem@nec.org', N'709-991-7311', N'976-755-4532', 2, 68195045, N'2014-07-28T13:03:56-07:00', N'lectus, a sollicitudin')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (40, 36, 975006, N'Yvonne', N'Copeland', N'ridiculus@disparturientmontes.com', N'798-704-7746', N'805-925-2440', 1, 91778315, N'2014-12-26T21:07:55-08:00', N'semper. Nam tempor')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (41, 37, 441200, N'Abbot', N'Alston', N'nisi@elit.co.uk', N'911-468-7272', N'752-284-3682', 1, 22681277, N'2014-06-17T07:05:49-07:00', N'Donec at arcu.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (42, 38, 279080, N'Fuller', N'Strickland', N'nascetur.ridiculus@consequatnecmollis.edu', N'421-529-4249', N'882-415-6847', 1, 85379005, N'2014-08-03T13:33:15-07:00', N'In at pede.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (43, 39, 956425, N'Kristen', N'Solis', N'odio.a@duinec.edu', N'109-162-3231', N'799-794-1890', 2, 81174841, N'2014-05-06T12:12:17-07:00', N'at pretium aliquet, metus urna')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (44, 40, 315189, N'Vincent', N'Avery', N'lacus.Ut.nec@nonlaciniaat.ca', N'336-259-9094', N'581-158-9751', 2, 55459224, N'2014-03-14T01:53:01-07:00', N'non ante bibendum')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (45, 41, 788277, N'Reece', N'Gillespie', N'Duis@disparturient.com', N'140-109-4647', N'775-212-2535', 1, 57988193, N'2014-10-28T22:52:32-07:00', N'erat eget ipsum. Suspendisse')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (46, 42, 279442, N'Maxwell', N'Christian', N'Mauris@dolor.co.uk', N'570-561-1411', N'658-211-9224', 1, 97591861, N'2014-08-27T14:40:06-07:00', N'elementum, dui quis')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (47, 43, 320822, N'Jescie', N'Gates', N'tortor@Maurisnondui.org', N'894-232-6492', N'483-754-1982', 2, 31114685, N'2014-02-23T13:04:57-08:00', N'commodo at, libero. Morbi accumsan')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (48, 44, 319981, N'Otto', N'Browning', N'non@diamlorem.net', N'193-293-7272', N'389-319-6727', 2, 92321003, N'2014-03-22T05:45:44-07:00', N'felis. Donec tempor,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (49, 45, 453489, N'Margaret', N'Baker', N'consectetuer.adipiscing@Inmi.net', N'601-387-6366', N'214-423-9178', 1, 74127883, N'2014-10-16T10:38:06-07:00', N'in consectetuer ipsum nunc id')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (50, 46, 526628, N'Flavia', N'Callahan', N'vehicula@gravidaPraesenteu.net', N'962-710-2036', N'158-142-0236', 2, 83997722, N'2014-12-11T12:27:29-08:00', N'magna nec quam.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (51, 47, 695179, N'Jaime', N'Clarke', N'sapien.cursus.in@necmalesuada.com', N'772-571-5010', N'858-754-2029', 1, 59736428, N'2014-05-09T06:50:06-07:00', N'amet nulla. Donec non justo.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (52, 48, 918271, N'Bertha', N'Bradley', N'Aliquam.rutrum.lorem@Donecegestas.net', N'669-365-9242', N'342-241-2412', 1, 82291351, N'2014-11-12T10:30:45-08:00', N'Sed malesuada augue')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (53, 49, 469499, N'Jackson', N'Vance', N'pellentesque@enimsit.com', N'188-178-8271', N'335-277-1534', 1, 26469350, N'2014-11-07T10:18:07-08:00', N'eu tellus. Phasellus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (54, 50, 233105, N'Nyssa', N'Palmer', N'lacus.Quisque.purus@velturpisAliquam.co.uk', N'854-439-7428', N'816-344-4902', 1, 63636220, N'2014-09-15T02:01:44-07:00', N'vitae diam. Proin')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (55, 51, 440368, N'Brenda', N'Bentley', N'Nunc.sed@Mauris.net', N'500-202-8938', N'902-825-9214', 2, 21715513, N'2014-10-03T03:59:31-07:00', N'nunc id enim. Curabitur massa.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (56, 52, 997731, N'Denton', N'Mooney', N'velit.Quisque@erat.ca', N'292-605-2647', N'361-765-0298', 1, 40507913, N'2014-05-16T23:00:13-07:00', N'amet luctus vulputate, nisi sem')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (57, 53, 615212, N'Timothy', N'Ware', N'feugiat.tellus.lorem@cursusaenim.org', N'703-869-8459', N'413-264-8985', 1, 99848909, N'2014-08-24T07:54:58-07:00', N'purus mauris a nunc.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (58, 54, 953503, N'Bruno', N'Hall', N'amet.risus.Donec@nonlaciniaat.com', N'240-327-6816', N'204-304-4773', 2, 32361278, N'2014-05-11T06:30:13-07:00', N'molestie orci tincidunt')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (59, 55, 786155, N'Ryan', N'Giles', N'Vivamus.nibh.dolor@montesnascetur.net', N'523-966-5349', N'594-296-0941', 2, 66814287, N'2014-05-20T16:12:58-07:00', N'sed libero. Proin sed turpis')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (60, 56, 327564, N'Shellie', N'Newton', N'gravida.sit.amet@vitaediam.co.uk', N'855-905-4773', N'834-984-3114', 2, 16873900, N'2014-08-26T02:32:06-07:00', N'Vivamus rhoncus. Donec est. Nunc')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (61, 57, 520193, N'Hedwig', N'Whitley', N'magna@natoquepenatibus.com', N'816-443-6985', N'534-900-5970', 1, 33685187, N'2014-08-28T00:37:23-07:00', N'non sapien molestie')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (62, 58, 157366, N'Ezekiel', N'Sosa', N'elementum.at.egestas@egestasblandit.ca', N'466-250-1887', N'532-374-2455', 1, 88289010, N'2014-11-07T15:48:16-08:00', N'lacinia mattis. Integer eu')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (63, 59, 620568, N'Quamar', N'Hoffman', N'quamar.at.hoffman@egestasblanditi.ca', N'423-143-5260', N'931-313-0472', 2, 96350038, N'2014-02-27T09:13:01-08:00', N'urna suscipit nonummy. Fusce fermentum')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (64, 60, 661937, N'Rhoda', N'Snider', N'dolor.Donec.fringilla@amet.co.uk', N'268-675-3987', N'165-873-5804', 2, 34296724, N'2014-12-27T04:41:48-08:00', N'Suspendisse aliquet, sem ut cursus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (65, 61, 797550, N'Alyssa', N'Everett', N'dui.in.sodales@Quisqueporttitoreros.org', N'728-844-3183', N'411-449-6275', 2, 48304373, N'2014-10-04T14:40:09-07:00', N'arcu et pede. Nunc sed')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (66, 62, 614218, N'Barrett', N'Bray', N'dictum@a.org', N'796-296-5249', N'328-509-4495', 1, 79302023, N'2014-04-01T12:51:38-07:00', N'libero. Proin sed turpis')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (67, 63, 771687, N'Wayne', N'Rosario', N'sed.dictum@pedemalesuadavel.edu', N'633-660-7255', N'941-257-5751', 1, 54899936, N'2014-02-02T22:44:49-08:00', N'Lorem ipsum dolor sit amet,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (68, 64, 228907, N'Gillian', N'Terry', N'eros.nec@diamPellentesque.net', N'755-625-8934', N'820-770-9118', 2, 96064424, N'2014-09-12T12:14:45-07:00', N'Proin dolor. Nulla semper tellus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (69, 65, 687159, N'Griffith', N'Barrett', N'elementum@temporeratneque.net', N'735-584-8912', N'124-898-8839', 1, 43520167, N'2014-06-16T22:21:35-07:00', N'consequat nec, mollis vitae, posuere')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (70, 66, 526253, N'Jemima', N'Conner', N'libero@aliquetnec.co.uk', N'129-597-4135', N'314-885-3126', 1, 58505422, N'2014-09-10T23:36:47-07:00', N'Fusce mollis. Duis')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (71, 67, 572072, N'Casey', N'Webb', N'lectus@NullafacilisiSed.ca', N'184-930-5896', N'960-468-5402', 1, 99157285, N'2014-10-23T12:12:01-07:00', N'nascetur ridiculus mus.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (72, 68, 738234, N'Abigail', N'Sears', N'faucibus.Morbi@insodales.ca', N'317-459-1354', N'641-239-1906', 2, 76446180, N'2014-10-30T04:30:46-07:00', N'lorem eu metus. In')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (73, 69, 654220, N'Simon', N'Hodges', N'Mauris.molestie.pharetra@iaculis.com', N'747-275-6058', N'922-722-7797', 1, 89539947, N'2014-06-30T23:40:37-07:00', N'urna justo faucibus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (74, 70, 881106, N'Hunter', N'Mckee', N'vel.faucibus.id@nullaDonecnon.net', N'868-120-2952', N'157-504-1785', 2, 54917707, N'2014-12-16T09:43:21-08:00', N'sem elit, pharetra ut,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (75, 71, 859515, N'Cleo', N'Acevedo', N'dis.parturient@pretiumetrutrum.ca', N'735-833-7280', N'670-428-3546', 1, 73360263, N'2014-08-21T14:03:51-07:00', N'tristique ac, eleifend')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (76, 72, 417580, N'Simon', N'morabih', N'simon.morabih@dev2000.com', N'(317)-459-1354', N'(317)-957-6999', 1, 78702276, N'2014-02-28T09:41:57-08:00', N'ultrices, mauris ipsum')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (77, 73, 437439, N'Hermione', N'Lott', N'Nullam.feugiat.placerat@interdumSedauctor.co.uk', N'485-378-3173', N'976-431-4825', 1, 53569432, N'2014-05-17T06:25:20-07:00', N'tincidunt, neque vitae semper')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (78, 74, 572374, N'Anjolie', N'Marquez', N'auctor@auguescelerisque.com', N'215-860-5136', N'969-895-4963', 2, 29694929, N'2014-11-28T02:51:55-08:00', N'volutpat nunc sit')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (79, 75, 833157, N'Ava', N'Casey', N'non.dapibus.rutrum@libero.org', N'192-271-2079', N'312-914-5900', 1, 66646391, N'2014-05-11T17:09:29-07:00', N'velit. Sed malesuada')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (80, 76, 179455, N'Olympia', N'Banks', N'urna.Vivamus.molestie@Curabitur.co.uk', N'595-915-7938', N'644-872-4748', 2, 98635545, N'2014-12-06T12:47:17-08:00', N'mi enim, condimentum eget,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (81, 77, 933228, N'Kasper', N'Whitaker', N'Morbi.metus.Vivamus@dictumeueleifend.org', N'359-765-9635', N'907-328-2374', 2, 55160016, N'2014-04-23T22:41:16-07:00', N'convallis erat, eget')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (82, 78, 327688, N'Simone', N'Bartlett', N'aliquam.arcu@sollicitudin.com', N'798-299-8655', N'457-547-9689', 2, 80295948, N'2014-08-05T12:44:13-07:00', N'Etiam laoreet, libero')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (83, 79, 447729, N'Dalton', N'Paul', N'Nunc.sollicitudin.commodo@Nulladignissim.co.uk', N'444-227-1786', N'763-610-3781', 2, 43029534, N'2014-05-28T13:50:07-07:00', N'ullamcorper magna. Sed eu eros.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (84, 80, 605018, N'Brittany', N'Melton', N'Nullam@aliquetliberoInteger.edu', N'512-360-2697', N'200-808-7910', 2, 56986614, N'2014-12-26T11:01:44-08:00', N'lorem, luctus ut,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (85, 81, 979450, N'Ryder', N'Maldonado', N'enim@Proin.edu', N'576-263-7199', N'420-476-6087', 1, 20753206, N'2014-08-25T14:50:06-07:00', N'interdum. Sed auctor')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (86, 82, 243837, N'Hasad', N'Swanson', N'quam.dignissim.pharetra@Maurismagna.ca', N'286-439-8595', N'169-918-5016', 1, 97819031, N'2014-05-11T07:10:01-07:00', N'ultrices a, auctor')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (87, 83, 122544, N'Cullen', N'Anderson', N'at@molestie.net', N'937-435-3432', N'882-922-1701', 1, 88884112, N'2014-08-10T02:17:22-07:00', N'sit amet diam')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (88, 84, 722676, N'Justina', N'Brock', N'ipsum.Suspendisse@hendrerita.com', N'595-211-2960', N'251-331-9405', 1, 84475801, N'2014-01-05T00:58:55-08:00', N'eros non enim')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (89, 85, 749173, N'Morgen', N'Kellersi', N'Mauris.vel.turpis@Cumsociisnatoque.ca', N'464-878-5176', N'678-258-7060', 1, 24156864, N'2014-08-17T02:34:29-07:00', N'tincidunt aliquam arcu. Aliquam')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (90, 86, 658129, N'Harrison', N'Aguirre', N'ultrices.iaculis.odio@molestieSed.com', N'907-124-2190', N'742-323-9985', 2, 82031999, N'2014-10-07T08:58:26-07:00', N'massa rutrum magna. Cras convallis')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (91, 87, 283685, N'Stella', N'Fields', N'lacus.Quisque.imperdiet@posuere.co.uk', N'113-379-6669', N'902-663-6267', 1, 38307224, N'2014-07-07T10:23:15-07:00', N'ac mattis ornare, lectus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (92, 88, 454901, N'Iliana', N'Houston', N'eu.dolor@nec.ca', N'923-977-6793', N'316-774-7685', 1, 81050682, N'2014-11-16T13:25:28-08:00', N'bibendum ullamcorper. Duis cursus, diam')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (93, 89, 715557, N'Ivana', N'Atkins', N'non.dui@mauris.co.uk', N'653-796-1017', N'686-893-3966', 2, 90598508, N'2014-03-08T21:14:38-08:00', N'nibh. Quisque nonummy')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (94, 90, 791201, N'Elaine', N'Fox', N'Suspendisse@atsemmolestie.com', N'665-626-6738', N'352-317-0122', 2, 90386392, N'2014-11-30T01:35:09-08:00', N'mi enim, condimentum eget,')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (95, 91, 137051, N'Daryl', N'Petty', N'dis.parturient@Fuscefermentumfermentum.co.uk', N'346-200-5460', N'977-815-7755', 2, 83860993, N'2014-10-25T15:16:39-07:00', N'pede blandit congue. In scelerisque')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (96, 92, 679957, N'Blaine', N'Davenport', N'erat@pretiumneque.ca', N'628-562-2928', N'457-395-2273', 1, 82391395, N'2014-07-13T22:56:09-07:00', N'Pellentesque ut ipsum')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (97, 93, 563957, N'Jade', N'Collins', N'vitae.dolor@orciUtsagittis.com', N'261-599-9683', N'904-929-0622', 1, 53372567, N'2014-01-27T21:47:17-08:00', N'vitae mauris sit')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (98, 94, 689635, N'Patience', N'Chan', N'dolor.nonummy@ac.net', N'947-424-6755', N'871-845-2489', 2, 16732007, N'2014-09-25T20:20:45-07:00', N'quis, pede. Praesent eu dui.')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (99, 95, 884893, N'Aiko', N'Brown', N'pulvinar.arcu@lobortis.org', N'542-730-7667', N'897-795-2267', 2, 68834032, N'2014-10-02T14:53:10-07:00', N'mi. Duis risus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (100, 96, 101559, N'Chastity', N'Herman', N'adipiscing@sodalespurus.net', N'158-126-6962', N'287-308-6548', 2, 80441720, N'2014-05-07T04:45:43-07:00', N'non justo. Proin')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (101, 97, 851194, N'Armando', N'Ballard', N'felis@montesnascetur.org', N'309-244-5557', N'940-610-4156', 2, 38511985, N'2014-07-17T12:58:56-07:00', N'eu enim. Etiam imperdiet dictum')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (102, 98, 253587, N'Fredericka', N'Cooley', N'interdum@mus.edu', N'387-611-9965', N'555-994-1251', 1, 77191463, N'2014-12-20T18:11:07-08:00', N'vehicula risus. Nulla eget metus')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (103, 99, 763313, N'April', N'Emerson', N'pede.ultrices.a@dolor.org', N'763-695-6512', N'594-449-6321', 2, 66078847, N'2014-07-12T03:57:12-07:00', N'pharetra. Quisque ac libero nec')
INSERT [dbo].[Sample] ([SampleID], [Customer_ID], [Order_Number], [Name_First], [Name_Last], [Email_Address], [Phonenumbr_01], [PhoneNumbr_02], [PhoneNumbr_Prefer], [Activity_ID], [Activity_Date], [Activity_Description]) VALUES (104, 100, 347919, N'Myra', N'Saunders', N'netus@Nullamvitae.co.uk', N'831-522-2203', N'843-515-8809', 2, 94493489, N'2014-11-26T04:32:46-08:00', N'Fusce dolor quam, elementum at,')
GO
print 'Processed 100 total records'
SET IDENTITY_INSERT [dbo].[Sample] OFF
/****** Object:  Table [dbo].[Names]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Names](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Context_GUID] [uniqueidentifier] NOT NULL,
	[Entity_ID] [int] NOT NULL,
	[Record_ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Signature] [char](64) NULL,
	[Process_ID] [nvarchar](25) NULL,
	[Log_GUID] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Names] ON
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2143, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 1, 61, N'Generic Issue', NULL, NULL, N'bcc8db69-2d63-4aa5-a36b-ec3ca9b2a8d1')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2144, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 27, N'Classification', NULL, NULL, N'54de521e-11c0-4b05-a6ce-b2ceda73f271')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2145, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 28, N'Originated On', NULL, NULL, N'8cf4a2a3-f0a7-4ab3-87ae-f1ad5758a871')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2146, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 29, N'Resolve By', NULL, NULL, N'3e4e35ce-fd73-40cf-a83f-71bfc9e1ccf0')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2147, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 30, N'Customer ID', NULL, NULL, N'0d4609a9-1f6f-4c46-8199-1153208fd0bb')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2148, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 31, N'Order Number', NULL, NULL, N'd094e477-613a-41a9-9fbe-905c0adc17d8')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2149, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 32, N'Description', NULL, NULL, N'74ceec8d-06e8-43a5-bc4c-813a84247f64')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2150, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 33, N'Status', NULL, NULL, N'02397c59-6951-4d26-b810-7017463603b9')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2151, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 34, N'Internal Note', NULL, NULL, N'891cc05e-9873-4b5b-bfe0-38e84de5375c')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2152, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 35, N'External Note', NULL, NULL, N'7a38b03b-6960-4585-91f4-c689489b5edd')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2153, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 36, N'Resolution Text', NULL, NULL, N'10d73125-bf8d-4a11-b0b2-ad20b0a53cb3')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2154, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 37, N'Resolution Code', NULL, NULL, N'b9abaa07-3b48-498b-b43e-ddee8854a72f')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2155, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 38, N'Adjustment Amount', NULL, NULL, N'cffac551-8c8f-46ea-9d48-9d065c19a578')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2156, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 39, N'Pending', NULL, NULL, N'11526ee7-e955-4613-a671-11f2c187bc70')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2158, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 40, N'Completed On', NULL, NULL, N'c72685c8-6caf-45e2-908a-e981c58d4c2b')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2159, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 1, 63, N'CRM Data', NULL, NULL, N'1d0cf1c2-d2f6-404d-a273-fb92d7111ade')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2160, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 41, N'Time Zone', NULL, NULL, N'2f3d6e17-93a0-46fb-8d85-39497cdee470')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2161, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 42, N'Source ID', NULL, NULL, N'd25af9c8-278c-4d89-b033-132d30366637')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2162, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 43, N'Source Order', NULL, NULL, N'f55214a1-83a0-43ed-9d5a-5123bec2b3d4')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2163, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 44, N'Activity ID', NULL, NULL, N'45be1a05-edfa-4ace-9f18-56c58d60c8bd')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2164, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 45, N'Name, First', NULL, NULL, N'251069d0-68c3-4954-a558-e29a0a5d4274')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2165, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 46, N'Name, Last', NULL, NULL, N'b249a6b1-9737-4f10-8fd7-e99d9f5c7e33')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2166, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 47, N'Name, Full', NULL, NULL, N'811feb88-b223-4f20-a34e-613052c027bb')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2167, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 48, N'Email Address', NULL, NULL, N'6b080885-008f-4ea0-bbfb-b8039a283e1c')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2168, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 49, N'PhoneDesc_01', NULL, NULL, N'af08689b-dcac-41d0-b1ae-b37dc4295283')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2169, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 50, N'PhoneDesc_02', NULL, NULL, N'b780c7ab-98ea-4f8a-92da-33d28da4ca1e')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2170, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 51, N'PhoneNmbr_01', NULL, NULL, N'462973aa-36ae-498c-80e0-2a9aaf9b1b19')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2171, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 52, N'PhoneNmbr_02', NULL, NULL, N'74d52740-52e5-449f-b938-21076a7efc77')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2172, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 53, N'PhoneExt_01', NULL, NULL, N'5dbc26f9-da07-4888-b186-dd1feeca2a27')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2173, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 54, N'PhoneExt_02', NULL, NULL, N'8096c2e9-12e3-49f7-addc-fe4fddac3bf9')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2174, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 55, N'PhoneNmbr_Prefer', NULL, NULL, N'0653ad57-172a-4579-b2bf-0e659ebf2bdf')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2177, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 56, N'Source: Email Address', NULL, NULL, N'9f0f49ba-7023-46b1-89b7-6eff37522a27')
INSERT [dbo].[Names] ([ID], [Context_GUID], [Entity_ID], [Record_ID], [Name], [Signature], [Process_ID], [Log_GUID]) VALUES (2178, N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', 2, 57, N'Source: Phone Number', NULL, NULL, N'390fc882-bdf4-4bdb-8782-ecfe50d91841')
SET IDENTITY_INSERT [dbo].[Names] OFF
/****** Object:  Table [dbo].[Log]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[GUID] [uniqueidentifier] NOT NULL,
	[User_Name] [nvarchar](100) NOT NULL,
	[User_Display] [nvarchar](100) NULL,
	[Entered] [datetime] NOT NULL,
	[Notes] [text] NULL,
	[Process_ID] [nvarchar](25) NULL,
	[Type] [nvarchar](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Filters]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Filters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[RegEx_Find] [text] NULL,
	[RegEx_Replace] [text] NULL,
	[Attribute_ID] [int] NULL,
	[Classification_ID] [int] NOT NULL,
	[Ordinal] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entities]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entities](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[GUID] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Entities] ([ID], [Name], [GUID]) VALUES (1, N'Classifications', 0)
INSERT [dbo].[Entities] ([ID], [Name], [GUID]) VALUES (2, N'Attributes', 0)
INSERT [dbo].[Entities] ([ID], [Name], [GUID]) VALUES (3, N'Values_Memos', 1)
INSERT [dbo].[Entities] ([ID], [Name], [GUID]) VALUES (4, N'Values_Numerics', 1)
INSERT [dbo].[Entities] ([ID], [Name], [GUID]) VALUES (5, N'Values_Text', 1)
/****** Object:  Table [dbo].[Contexts]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contexts](
	[GUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Source_Name] [nvarchar](100) NOT NULL,
	[Purpose] [text] NOT NULL,
	[Revert_SQL] [nvarchar](100) NULL,
	[Revert_EXE] [nvarchar](100) NULL,
	[Signature] [char](64) NULL,
	[User_ID] [nvarchar](100) NULL,
	[User_Display] [nvarchar](100) NULL,
	[Installed] [datetime] NOT NULL,
	[User_Address] [nvarchar](100) NULL,
	[Enable] [datetime] NULL,
	[Disable] [datetime] NULL,
	[Parent_GUID] [uniqueidentifier] NULL,
	[Activated] [datetime] NULL,
	[Released] [datetime] NULL,
	[Source_URI] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Contexts] ([GUID], [Name], [Source_Name], [Purpose], [Revert_SQL], [Revert_EXE], [Signature], [User_ID], [User_Display], [Installed], [User_Address], [Enable], [Disable], [Parent_GUID], [Activated], [Released], [Source_URI]) VALUES (N'0d50b5e6-5c81-4285-9919-d2bf82aa5554', N'English, U.S.', N'ININ MarketPlace', N'Translation', NULL, NULL, NULL, NULL, NULL, CAST(0x0000A32800000000 AS DateTime), NULL, CAST(0x0000A32800000000 AS DateTime), NULL, NULL, CAST(0x0000A32800000000 AS DateTime), CAST(0x0000A32800000000 AS DateTime), N'http://marketplace.inin.com')
/****** Object:  Table [dbo].[Communication]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Communication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Entered] [datetime] NOT NULL,
	[Subject] [nvarchar](500) NULL,
	[Process_ID] [nvarchar](25) NULL,
	[User_ID] [nvarchar](250) NULL,
	[User_Display] [nvarchar](250) NULL,
	[Message] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Com_Report]    Script Date: 08/19/2014 07:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[Com_Report]    Script Date: 08/19/2014 12:31:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Com_Report] AS 
SELECT     *
FROM         (SELECT     [Communication_ID], [Name], CASE WHEN [Name] IN ('Originated On', 'Resolve By', 'Pending', 'Completed On') AND [Value] IS NOT NULL 
                                              THEN CAST(CAST(CAST([Value] AS CHAR(19)) + 'Z' AS DATETIME) AS VARCHAR(MAX)) ELSE [Value] END AS [Value]
                       FROM          (SELECT     [Communication_ID], [Name], [Value]
                                               FROM          (SELECT     [Communication_ID], t_n.[Name], t_n.[Record_ID], [Value]
                                                                       FROM          (SELECT     t_ca.[Communication_ID], t_ca.[ID], t_ca.[Attribute_ID], [Record_ID], [Value]
                                                                                               FROM          (SELECT     t_l.[Entered], [Record_ID], [Value], [Record_GUID]
                                                                                                                       FROM          (SELECT     t_v.[Record_ID], t_vt.[Value], t_v.[Log_GUID], t_v.[Record_GUID]
                                                                                                                                               FROM          [Values_Text] AS t_vt INNER JOIN
                                                                                                                                                                      [Values] AS t_v ON (t_vt.[GUID] = t_v.[Record_GUID])) AS s_v INNER JOIN
                                                                                                                                              [Log] AS t_l ON (t_l.[GUID] = s_v.[Log_GUID])) AS s_l INNER JOIN
                                                                                                                      [Com_Attr] AS t_ca ON (t_ca.[ID] = [Record_ID])) AS s_ca INNER JOIN
                                                                                              [Names] AS t_n ON (t_n.[Record_ID] = s_ca.[Attribute_ID])) AS s_n INNER JOIN
                                                                      [Communication] AS t_c ON (t_c.[ID] = s_n.[Communication_ID])) AS s_c
                       UNION
                       SELECT     u_ca.[Communication_ID], 'Agent Name' AS [Name], (CASE [User_Display] WHEN NULL THEN [User_Name] ELSE [User_Display] END) AS [Value]
                       FROM         [Log] AS u_l INNER JOIN
                                             [Values] AS u_v ON (u_l.[GUID] = u_v.[Log_GUID]) INNER JOIN
                                             [Com_Attr] AS u_ca ON (u_ca.[ID] = u_v.[Record_ID])) AS s_all PIVOT (MAX([Value]) FOR [Name] IN ([Adjustment Amount], [Classification], [Customer ID], 
                      [Order Number], [Originated On], [Pending], [Resolution Code], [Resolve By], [Completed On], [Agent Name], [Source: Email Address], [Source: Phone Number], 
                      [Status])) AS pvt
 


GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Com_Report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Com_Report'
GO
/****** Object:  Default [DF__Com_Class__Enter__48CFD27E]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Com_Class] ADD  DEFAULT (getdate()) FOR [Entered]
GO
/****** Object:  Default [DF__Communica__Enter__4D94879B]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Communication] ADD  DEFAULT (getdate()) FOR [Entered]
GO
/****** Object:  Default [DF__Contexts__Instal__3A81B327]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Contexts] ADD  DEFAULT (getdate()) FOR [Installed]
GO
/****** Object:  Default [DF__Log__GUID__52593CB8]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Log] ADD  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF__Log__Entered__534D60F1]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Log] ADD  DEFAULT (getdate()) FOR [Entered]
GO
/****** Object:  Default [DF__Values_Mem__GUID__59FA5E80]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Values_Memos] ADD  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF__Values_Num__GUID__5EBF139D]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Values_Numerics] ADD  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF__Values_Tex__GUID__6383C8BA]    Script Date: 08/19/2014 07:51:56 ******/
ALTER TABLE [dbo].[Values_Text] ADD  DEFAULT (newid()) FOR [GUID]
GO
